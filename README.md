# README #

Khemlabs - Kickstarter - Socket importer

### What is this repository for? ###

This lib is intended for developers that are using the khemlabs kickstarter framework, it
is a rapper of the socket.io lib

### Requirements ###

> Khemlabs kickstarter server

> socket.io
    
> socketio-jwt
    
> fs
    
> khem-log
  
### Before using it ###

> Uncomment this lines at app.js

  > var ss = require('khem-socket');
  
  > global.__ws = new ss()
  
  > __ws.configure( server, __config.SOCKET_IGNORE, __config.SECRET_PRHASE );
  
> Create a websocket endpoint in server/websockets

  > Example at server/websockets/example.js

### Who do I talk to? ###

* dnangelus repo owner and admin
* developer elgambet and khemlabs