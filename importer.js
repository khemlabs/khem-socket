var fs = require('fs'),
	log = require('khem-log');

/**
* eSocketImporter imports all files with format [model].socket.js
* witch are in the directory sockets/
*
* IGNORE FILES
* ------------
* You can add files into the ignore list running the lift command with the environment variable
* SOCKET_IGNORE which receives a comma separated list of names
*/
var eSocketImporter = function() {
	var self = this;

	this.sockets = {};

	this.filenames = [];
	this.socketsNAMES = [];

	this.isIgnoreFile = function(filename, ignore) {
		return ignore.indexOf(filename) >= 0;
	};

	this.validFileName = function(filename, ignore) {
		var name = filename.split('.');
		if (!self.isIgnoreFile(filename, ignore) && name.length == 2 && name[1] == 'js') return name[0];
		else {
			if (!self.isIgnoreFile(filename, ignore)) {
				log.warning(
					'The file ' + filename + ' has an invalid name for a socket',
					'khem-socket/importer',
					'validFileName'
				);
			} else {
				log.info('Socket importer: The file ' + filename + ' is in the ignore file array');
			}
			return false;
		}
	};

	this.checkFiles = function(path, ignore) {
		self.filenames = fs.readdirSync(path);
		for (var i = 0; i < self.filenames.length; i++) {
			var filename = self.filenames[i];
			var name = self.validFileName(filename, ignore);
			if (name) {
				log.info('Fetching socket file: ' + filename);
				try {
					self.sockets[name] = require(path + filename);
					self.socketsNAMES.push(name);
				} catch (err) {
					log.error(err, 'khem-socket/importer', 'checkFiles');
				}
			}
		}
		return self.sockets;
	};

	this.getSockets = function(path, ignore) {
		log.info('Importing websockets endpoints...');
		if (!self.socketsNAMES.length) self.checkFiles(path, ignore);
	};
};

module.exports = new eSocketImporter();
