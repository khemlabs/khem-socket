var util         = require('util'),
    EventEmitter = require('events').EventEmitter,
    importer     = require('./importer'),
    log          = require('khem-log');

var kssocket = function(){
  
  var self        = this;
  
  this.configured = false;  
  this.server     = false;
  this.io         = false;

  this.checkIgnoreFiles = function( ignore ) {
    if(process.env.SOCKET_IGNORE){
      var ignoreFiles = process.env.SOCKET_IGNORE.split(',');
      for(var i = 0; i < ignoreFiles.length; i++){
        ignore.push(ignoreFiles[i]);
      }
    }
    return ignore;
  };
  
  this.configure = function( server, ignore, SECRET_PRHASE ){
    var path = __dirname.substring(0, __dirname.indexOf('node_modules')) + 'server/websockets/';
    ignore = self.checkIgnoreFiles(ignore);
    
    self.server       = server;
    self.io           = require("socket.io")(self.server);
    self.socketioJwt  = require("socketio-jwt");
    self.socket       = null;
    self.rooms        = {
      default: {
        users: []
      }
    };
    self.users = {
      default: {
        rooms: []  
      }
    };
    
    self.io.use(self.socketioJwt.authorize({secret: SECRET_PRHASE, handshake: true}));
    
    importer.getSockets( path, ignore );
    self.configured = true;
    self.emit('configured');
    self.onConnect();
  };
  
  this.onConnect = function(){
    self.io.on('connection', function (socket) {
      var user = self.getUser(socket);
      if(!self.users[user.username]) self.users[user.username] = {rooms: []};
      self.emit('connection', {socket: socket, user: user});
      socket.on('disconnect', function() {
        if(self.users[user.username]){
          self.users[user.username].rooms.forEach(function(room){
            self.leaveRoom(room, socket);
          });
          delete self.users[user.username];
        }
      });
    }, function(err){
      log.error(err, 'khem-socket/index', 'connect');
    });
  };
  
  this.roomEmit = function(room, event, data){
    self.io.to(room).emit(event, data);
  };
  
  this.roomsEmit = function(rooms, event, data){
    rooms.forEach(function(room){
      self.roomEmit(room, event, data);
    });
  };
  
  this.joinRoom = function(room, socket){
    socket.join(room);
    var user = self.getUser(socket).username;
    if(!self.rooms[room]) self.rooms[room] = {users: []};
    if(user && self.rooms[room].users.indexOf(user) == -1){
      self.rooms[room].users.push(user);
      self.users[user].rooms.push(room);
      self.roomEmit(room, 'room-joined', {room: room, user: user});
    }
  };
  
  self.leaveRoom = function(room, socket){
    socket.leave(room);
    var user = self.getUser(socket).username;
    if(user && self.rooms[room] && self.rooms[room].users.indexOf(user) >=0){
      self.rooms[room].users.splice(self.rooms[room].users.indexOf(user), 1);
      if(self.users[user] && self.users[user].rooms.indexOf(room) >= 0) 
        self.users[user].rooms.splice(self.users[user].rooms.indexOf(room), 1);
      if(self.rooms[room].users.length == 0) delete self.rooms[room];
      self.roomEmit(room, 'room-left', {room: room, user: user});
    }
  };
  
  this.getRoomUsers = function(room){
    var users = [];
    if(self.rooms[room] && self.rooms.users)
      users = self.rooms.users;
    return users;
  };
  
  this.emitRoomUsers = function(room, socket){
    socket.emit('room-users', self.getRoomUsers(room));  
  };
  
  this.emitRoomUsersToAll = function(room){
    self.roomEmit(room, 'room-users', self.getRoomUsers(room));
  };
  
  this.getUser = function(socket){
   return socket.decoded_token || {}; 
  }
};

// extend the EventEmitter class using our Radio class
util.inherits(kssocket, EventEmitter);

module.exports = kssocket;
